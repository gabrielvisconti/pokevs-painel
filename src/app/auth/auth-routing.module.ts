import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NbAuthComponent } from '@nebular/auth';  

import { NgxLoginComponent } from './login/login.component';

import { PagesComponent } from '../pages/pages.component';

export const routes: Routes = [
  {
    path: '',
    component: NbAuthComponent, 
    children: [
      {
        path: 'login',
        component: NgxLoginComponent, // <---
      }
    ], // <---
  },
  { path: 'pages/iot-dashboard', 
    loadChildren: 'app/pages/pages.module#PagesModule' 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgxAuthRoutingModule {
}