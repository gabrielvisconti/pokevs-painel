import { Component, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';
import { ConfigService } from '../../services/config/config.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class NgxLoginComponent {

	adminId: any;
	adminEmail: any;
	adminPasscode: any;
	loginResponse: any;
	wrong: any;
	loading: boolean = false;

	constructor (public router: Router, 
				 private route: ActivatedRoute,
				 public configService: ConfigService,
				 public authService: AuthService) 
	{ 
	}

	loginTemp() {
	    this.router.navigateByUrl('pages/dashboard').then(nav => {
	    console.log(nav); // true if navigation is successful
	  }, err => {
	    console.log(err) // when there's an error
	  });
	}


	login() {
		if(this.adminEmail && this.adminPasscode) {
			this.loading = true;
			this.authService.login(this.adminEmail, this.adminPasscode)
		  	.toPromise()
		      .then(data => {
		      	console.log(data);
				this.loading = false;
	        	this.loginResponse = data.text();
		        if (this.loginResponse == 'wrong') {	
		        	this.wrong = true;	        	
		        } else {
		        	this.authService.adminInfos = JSON.parse(this.loginResponse);
					this.router.navigateByUrl('pages/users');
			  	}
		      }, err => {
				this.loading = false;
		      	console.log(err);
		    });
	    } else {
		    console.log("erro");
	    }
	}

}