import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';

import { DashboardModule } from './dashboard/dashboard.module';
import { UsersModule } from './users/users.module';
import { PollsModule } from './polls/polls.module';
import { PollModule } from './poll/poll.module';
import { AddUserModule } from './add-user/add-user.module';
import { UserModule } from './user/user.module';
import { ArticlesModule } from './articles/articles.module';
import { ArticleModule } from './article/article.module';


@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    UsersModule,
    PollsModule,
    PollModule,
    AddUserModule,
    UserModule,
    ArticlesModule,
    ArticleModule
  ],
  declarations: [
    PagesComponent,
  ],
})
export class PagesModule {
}
