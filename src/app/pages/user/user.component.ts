import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';

import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'ngx-user',
  styleUrls: ['./user.component.scss'],
  templateUrl: './user.component.html',
})

export class UserComponent implements OnDestroy {

    private alive = true;
    edit: any = false;
    alertEmail: any;
    isLoadingAlertActive: any = false;
    isSuccessAlertActive: any = false;

    constructor(public userService: UserService, 
                private route: ActivatedRoute,
                public router: Router) 
    {
      console.log(this.userService.selectedUser);
    }

    ngOnDestroy() {
        this.alive = false;
        this.isSuccessAlertActive = false;  
        this.userService.selectedUser = null;
        this.userService.editedUser = null;
    }

    changeInterface() {
      this.isSuccessAlertActive = false; 
      this.userService.editedUser = this.userService.selectedUser;
      this.edit = !this.edit;
    }

    async editUser() {
        this.isLoadingAlertActive = true;
        await this.userService.editUser()
            .toPromise()
                .then(data => {
                    console.log(data.text());
                    if (data.text() == "used") {
                        this.alertEmail = true;
                    } else {                 
                        this.userService.selectedUser = this.userService.editedUser;
                        this.edit = !this.edit;
                        this.isLoadingAlertActive = false;
                        this.isSuccessAlertActive = true;  
                    }                
                }, err => {
                    console.log(err);
                });
    }

    deleteUser() {
         this.userService.deleteUser()
            .toPromise()
                .then(data => {
                    this.router.navigateByUrl('/pages/users');                     
                }, err => {
                    console.log(err);
                })
        }

    onDeleteConfirm(): void {
        if (window.confirm('Tem certeza que deseja apagar?')) {
            this.deleteUser();
        } else {
            
        }
    }

    deletePoll() {
        this.userService.deleteUser()
            .toPromise()
                .then(data => {       
                    this.router.navigateByUrl('/pages/users'); 
                    console.log(data);
        }, err => {
            console.log(err);
        });
    }
}
