import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { PollComponent } from './poll/poll.component';
import { PollsComponent } from './polls/polls.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UserComponent } from './user/user.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleComponent } from './article/article.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'article',
    component: ArticleComponent,
  }, {
    path: 'articles',
    component: ArticlesComponent,
  }, {
    path: 'poll',
    component: PollComponent,
  }, {
    path: 'polls',
    component: PollsComponent,
  }, {
    path: 'add-user',
    component: AddUserComponent,
  }, {
    path: 'user',
    component: UserComponent,
  }, {
    path: 'users',
    component: UsersComponent,
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
