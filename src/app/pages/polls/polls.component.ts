import { Component, OnDestroy, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';

import { PollService } from '../../services/poll/poll.service';

import { AddPollComponent } from './add-poll/add-poll.component';

@Component({
  selector: 'ngx-polls',
  styleUrls: ['./polls.component.scss'],
  templateUrl: './polls.component.html',
})

export class PollsComponent implements OnDestroy {

    private alive = true;

    settings = {
        noDataMessage: 'Nenhuma enquete para mostrar',
        hideSubHeader: false,
        hideHeader: true,
        actions: {
            delete: false,
            edit: false,
            add: false,
        }, 
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        columns: {
            questionTitle: {
                title: 'Buscar por título',
                type: 'string',
            }
        }
    };

    source: LocalDataSource = new LocalDataSource();

    constructor(private service: SmartTableData,
                public router: Router, 
                private dialogService: NbDialogService,
                public pollService: PollService) 
    {
        this.getPolls();
    }

    ngOnDestroy() {
        this.alive = false;
    }

    selectPoll(poll) {
        this.pollService.selectedPoll = poll.data;
        this.router.navigateByUrl('pages/poll').then(nav => {
            console.log(nav);
        }, err => {
            console.log(err);
        });
    }

    openDialogAddPoll() {
        this.dialogService.open(AddPollComponent).onClose
            .subscribe(data => {
                this.getPolls();
            });
    }

    getPolls() {
        this.pollService.getPolls()
            .toPromise()
                .then(data => {
                    console.log(data);
                    let companies = data;
                    this.source.load(companies);
        }, err => {
            console.log(err);
        });
    }

    // onDeleteConfirm(event): void {
    //     if (window.confirm('Are you sure you want to delete?')) {
    //         event.confirm.resolve();
    //         } else {
    //         event.confirm.reject();
    //     }
    // }
}