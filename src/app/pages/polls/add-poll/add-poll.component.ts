import { Component, TemplateRef } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';

import { PollService } from '../../../services/poll/poll.service';

@Component({
  selector: 'ngx-add-poll',
  templateUrl: 'add-poll.component.html',
  styleUrls: ['add-poll.component.scss'],
})

export class AddPollComponent {

    imageSrc: any;

    constructor(protected ref: NbDialogRef<AddPollComponent>,
                public router: Router, 
                public pollService: PollService)  
    {}

    cancel() {
      this.ref.close();
    }

    ngOnDestroy() {
        this.pollService.newPoll.questionTitle = "" ;
        this.pollService.newPoll.questionDescription = "";
    }

    addPollQuestion() {
        if (this.pollService.newPoll.questionTitle && 
            this.pollService.newPoll.questionDescription)
        {
            this.pollService.addPoll()
            .toPromise()
              .then(data => {
                let response = data.text();
                console.log(response);   
                this.ref.close();
              }, err => {
                console.log(err);
            });
        } else {
           console.log("erro de conexão");
        }
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
    }

    _handleReaderLoaded(e) {
        let reader = e.target;
        this.imageSrc = reader.result;
        console.log(this.imageSrc);
        let questionImage = this.imageSrc.replace('data:image/png;base64,', '');
        this.pollService.newPoll.questionImage = questionImage.replace('data:image/jpeg;base64,', '');
    }
}
