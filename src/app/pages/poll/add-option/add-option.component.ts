import { Component, TemplateRef } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';

import { PollService } from '../../../services/poll/poll.service';

@Component({
  selector: 'ngx-add-option',
  templateUrl: 'add-option.component.html',
  styleUrls: ['add-option.component.scss'],
})

export class AddOptionComponent {

  constructor(protected ref: NbDialogRef<AddOptionComponent>,
              public router: Router, 
              public pollService: PollService)  
  {

  }

    cancel() {
      this.ref.close();
    }

    ngOnDestroy() {
        this.pollService.newOption.optionTitle = "" ;
    }

    addPollOption() {
        if (this.pollService.newOption.optionTitle && this.pollService.selectedPoll.questionId)
        {
            this.pollService.addOption()
            .toPromise()
              .then(data => {
                let response = data.text();
                console.log(response);   
                this.ref.close();
              }, err => {
                console.log(err);
            });
        } else {
           console.log("erro de conexão");
        }
    }
}
