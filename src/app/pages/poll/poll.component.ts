import { Component, OnDestroy, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';

import { PollService } from '../../services/poll/poll.service';
import { AddOptionComponent } from './add-option/add-option.component';
import { OptionComponent } from './option/option.component';

@Component({
  selector: 'ngx-poll',
  styleUrls: ['./poll.component.scss'],
  templateUrl: './poll.component.html',
})

export class PollComponent implements OnDestroy {

    private alive = true;

    isEditAllowed: boolean = false;

    settings = {
        title: 'Opções',
        noDataMessage: 'Nenhuma opção para mostrar',
        hideSubHeader: false,
        hideHeader: true,
        actions: {
            columnTitle: 'Ações',
            delete: false,
            edit: false,
            add: false,
        }, 
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        columns: {
            optionTitle: {
                title: 'Opções',
                type: 'string',
            },
            voteCount: {
                title: 'Votos',
                type: 'string',
            }
        }
    };

    source: LocalDataSource = new LocalDataSource();
    imageSrc: any;

    constructor(private service: SmartTableData,
                public router: Router, 
                private dialogService: NbDialogService,
                public pollService: PollService) 
    {
        this.getOptions();
    }

    allowEdit() {
        this.isEditAllowed = true;
        this.pollService.editedPoll = this.pollService.selectedPoll;
    }

    ngOnDestroy() {
        this.alive = false;
    }

    editPoll() {
        if (this.pollService.editedPoll.questionTitle && 
            this.pollService.editedPoll.questionDescription)
        {
            this.pollService.editPoll()
            .toPromise()
              .then(data => {
                let response = JSON.parse(data.text());
                console.log(response);   
                this.pollService.selectedPoll = response;
                this.isEditAllowed = false;
              }, err => {
                console.log(err);
            });
        } else {
           console.log("erro de conexão");
        }
    }

    selectOption(option) {
        this.pollService.selectedOption = option.data;
        this.pollService.editedOption = option.data;
        this.dialogService.open(OptionComponent).onClose.subscribe(data => {
            this.getOptions();
        });
    }

    openDialogAddOption() {
        this.dialogService.open(AddOptionComponent).onClose
            .subscribe(data => {
                this.getOptions();
            });
    }

    getOptions() {
        this.pollService.getOptionsByQuestion()
            .toPromise()
                .then(data => {
                    let options = JSON.parse(data.text());
                    this.source.load(options);
                    console.log(options);
        }, err => {
            console.log(err);
        });
    }

    onDeleteConfirm(): void {
        if (window.confirm('Tem certeza que deseja apagar?')) {
            this.deletePoll();
        } else {
            
        }
    }

    deletePoll() {
        this.pollService.deletePoll()
            .toPromise()
                .then(data => {       
                    this.router.navigateByUrl('/pages/polls'); 
                    console.log(data);
        }, err => {
            console.log(err);
        });
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
    }

    _handleReaderLoaded(e) {
        let reader = e.target;
        this.imageSrc = reader.result;
        console.log(this.imageSrc);
        let questionImage = this.imageSrc.replace('data:image/png;base64,', '');
        this.pollService.editedPoll.questionImage = questionImage.replace('data:image/jpeg;base64,', '');
    }
}