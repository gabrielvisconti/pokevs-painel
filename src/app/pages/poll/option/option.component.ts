import { Component, TemplateRef } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';

import { PollService } from '../../../services/poll/poll.service';

@Component({
  selector: 'ngx-option',
  templateUrl: 'option.component.html',
  styleUrls: ['option.component.scss'],
})

export class OptionComponent {

    settings = {
        title: 'Opções',
        noDataMessage: 'Nenhum voto para mostrar',
        hideSubHeader: false,
        hideHeader: true,
        actions: {
            columnTitle: 'Ações',
            delete: false,
            edit: false,
            add: false,
        }, 
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        columns: {
            userName: {
                title: 'Associado',
                type: 'string',
            }
        }
    };

    source: LocalDataSource = new LocalDataSource();

    edit: boolean = false;

    constructor(protected ref: NbDialogRef<OptionComponent>,
                public router: Router, 
                   public pollService: PollService)  
    {
    }

    cancel() {
      this.ref.close();
    }

    ngAfterViewInit() {
      this.getVotesByOption();
    }

    ngOnDestroy() {
        this.pollService.editedOption = null;
        this.pollService.selectedOption = null;
    }

    changeInterface() {
        this.edit = !this.edit;
    }

    getVotesByOption() {
        this.pollService.getVotesByOption()
            .toPromise()
                .then(data => {
                    let votes = JSON.parse(data.text());
                    this.source.load(votes);
                    console.log(votes);
        }, err => {
            console.log(err);
        });
    }

    updateOption() {
        if (this.pollService.editedOption.optionTitle && this.pollService.editedOption.optionId) {
            this.pollService.editOption()
            .toPromise()
              .then(data => {
                let response = data.text();
                this.ref.close();
              }, err => {
                console.log(err);
            });
        } else {
           console.log("erro de conexão");
        }
    }
}
