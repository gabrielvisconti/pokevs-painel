import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';

import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'ngx-users',
  styleUrls: ['./users.component.scss'],
  templateUrl: './users.component.html',
})

export class UsersComponent implements OnDestroy {

    private alive = true;

    settings = {
            noDataMessage: 'Nenhum associado para mostrar',
            actions: {
                delete: false,
                edit: false,
                add: false,
            }, 
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            columns: {
                userName: {
                    title: 'Nome',
                    type: 'string',
                },
                userEmail: {
                    title: 'E-mail',
                    type: 'string',
                },
                userPhone: {
                    title: 'Telefone',
                    type: 'string',
                }
        },
    };

    users = [];
    source: LocalDataSource = new LocalDataSource();

    constructor(private service: SmartTableData,
                public router: Router, 
                private route: ActivatedRoute,
                public userService: UserService) 
    {
        this.getUsers();
    }

    ngOnDestroy() {
        this.alive = false;
    }

    openAddUserPage() {
            this.router.navigateByUrl('pages/add-user').then(nav => {
        }, err => {
        });
    }

    selectUser(user) {
        console.log(user);
        this.userService.selectedUser = user.data;
        this.router.navigate(['/pages/user']);
    }

    getUsers() {
        this.userService.getUsers()
            .toPromise()
                .then(data => {
                    console.log(data);
                    let users = data;
                    this.source.load(users);
        }, err => {
        console.log(err);
        });
    }

    // onDeleteConfirm(event): void {
    //     if (window.confirm('Are you sure you want to delete?')) {
    //         event.confirm.resolve();
    //         } else {
    //         event.confirm.reject();
    //     }
    // }
}
