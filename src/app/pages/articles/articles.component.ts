import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';

import { ArticlesService } from '../../services/articles/articles.service';

@Component({
  selector: 'ngx-articles',
  styleUrls: ['./articles.component.scss'],
  templateUrl: './articles.component.html',
})

export class ArticlesComponent implements OnDestroy {

    private alive = true;

    settings = {
            noDataMessage: 'Nenhum associado para mostrar',
            actions: {
                delete: false,
                edit: false,
                add: false,
            }, 
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            columns: {
                articleTitle: {
                    title: 'Nome',
                    type: 'string',
                },
                articleRegisterDatetime: {
                    title: 'Data de registro',
                    type: 'string',
                }
        },
    };

    articles = [];
    source: LocalDataSource = new LocalDataSource();

    constructor(private service: SmartTableData,
                public router: Router, 
                private route: ActivatedRoute,
                public articlesService: ArticlesService) 
    {
        this.getArticles();
    }

    ngOnDestroy() {
        this.alive = false;
    }

    openAddUserPage() {
            this.router.navigateByUrl('pages/add-user').then(nav => {
        }, err => {
        });
    }

    selectArticle(article) {
        console.log(article);
        this.articlesService.selectedArticle = article.data;
        this.router.navigate(['/pages/article']);
    }

    getArticles() {
        this.articlesService.getArticles()
            .toPromise()
                .then(data => {
                    console.log(data);
                    let articles = data;
                    this.source.load(articles);
        }, err => {
        console.log(err);
        });
    }

    // onDeleteConfirm(event): void {
    //     if (window.confirm('Are you sure you want to delete?')) {
    //         event.confirm.resolve();
    //         } else {
    //         event.confirm.reject();
    //     }
    // }
}
