import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Lista de usuários',
    icon: 'people-outline',
    link: '/pages/users',
    home: true,
  },
  {
    title: 'Artigos',
    icon: 'bar-chart-outline',
    link: '/pages/articles',
    home: true,
  },
  {
    title: 'Sair',
    icon: 'log-out',
    link: '/auth/login',
    home: true,
  }
];
