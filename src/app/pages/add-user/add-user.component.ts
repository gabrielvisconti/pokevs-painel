import { Component, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';

import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'ngx-add-user',
  styleUrls: ['./add-user.component.scss'],
  templateUrl: './add-user.component.html',
})

export class AddUserComponent implements OnDestroy {

    private alive = true;
    isEmailExist: any = false;
    isAllinPutFilled: any = false;

    constructor(public userService: UserService,
                public router: Router, 
                private route: ActivatedRoute) 
    {
    }

    ngOnDestroy() {
        this.alive = false;
        this.userService.newUser = {};
    }

    addUser() {
        this.isEmailExist = false;
        this.isAllinPutFilled = false;
        if (this.userService.newUser.userName && 
            this.userService.newUser.userEmail &&
            this.userService.newUser.userPasscode &&
            this.userService.newUser.userPhone &&
            this.userService.newUser.userCode &&
            this.userService.newUser.userBirthdate)
        {
            this.userService.addUser()
            .toPromise()
              .then(data => {
                let response = data.text();
                console.log(response);
                if (response == 'used') {
                    this.isEmailExist = true; 
                } else {      
                    this.router.navigateByUrl('/pages/users'); 
                    this.userService.newUser = {};
                }                
              }, err => {
                console.log(err);
            });
        } else {
            this.isAllinPutFilled = true;   
        }
    }
}
