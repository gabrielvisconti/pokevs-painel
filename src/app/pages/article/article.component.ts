import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Router, ActivatedRoute  } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';

import { ArticlesService } from '../../services/articles/articles.service';

@Component({
  selector: 'ngx-article',
  styleUrls: ['./article.component.scss'],
  templateUrl: './article.component.html',
})

export class ArticleComponent implements OnDestroy {
    
    private alive = true;

    constructor(private service: SmartTableData,
                public router: Router, 
                private route: ActivatedRoute,
                public articlesService: ArticlesService) 
    {
    }

    ngOnDestroy() {
        this.alive = false;
    }

    // onDeleteConfirm(event): void {
    //     if (window.confirm('Are you sure you want to delete?')) {
    //         event.confirm.resolve();
    //         } else {
    //         event.confirm.reject();
    //     }
    // }
}
