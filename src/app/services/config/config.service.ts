import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  
  driver: any = {};

  baseURL: string = "http://www.pokeversus.com/api/admin/";

  constructor(public http: Http) 
  { 
  }

  insert(str, index, value) {
      return str.substr(0, index) + value + str.substr(index);
  }

  // fazerLogin(driverEmail, driverPasscode) {
  //   let link = this.baseURL + 'login.php';
  //   let body = {
  //         driverEmail: driverEmail, 
  //         driverPasscode: driverPasscode
  //     };
  //   let headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.http.post(link, body, {headers: headers});
  // }

}
