import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpClientModule, } from '@angular/common/http';
import { ConfigService } from '../config/config.service'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    adminId: any;
    adminEmail: any;
    adminPasscode: any;
    adminInfos: any;


    constructor(public http: Http,
                public httpClient: HttpClientModule,
                public configService: ConfigService) 
    { 
    }

    login(adminEmail, adminPasscode) {
        let link = this.configService.baseURL + 'login.php';
        let body = {
            adminEmail: adminEmail, 
            adminPasscode: adminPasscode
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

}
