import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ConfigService } from '../config/config.service'
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

    userId: any = {};
    newArticle: any = {};
    selectedArticle: any = {};
    editedArticle: any = {};
    
    alertArticleAdd: any = false;
    isSuccessAlertActive: any = false;

    constructor(public http: Http,
                public configService: ConfigService) 
    { 
    }
        
    getArticles() {
        let URL = this.configService.baseURL + 'articles.php';
        return this.http.get(URL).map(res => res.json());;
    }

}
