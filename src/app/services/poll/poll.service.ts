import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ConfigService } from '../config/config.service'

@Injectable({
  providedIn: 'root'
})
export class PollService {

    isSuccessAlertActive: any = false;

    newPoll: any = {};
    selectedPoll: any = {};
    editedPoll: any = {};

    newOption: any = {};
    selectedOption: any = {};
    editedOption: any = {};

    constructor(public http: Http,
                public configService: ConfigService) 
    { 
    }

    deletePoll() {
        let link = this.configService.baseURL + 'delete-poll.php';
        let body = {
            questionId: this.selectedPoll.questionId
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    getVotesByOption() {
        let link = this.configService.baseURL + 'votes-by-option.php';
        let body = {
            optionId: this.selectedOption.optionId
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    getOptionsByQuestion() {
        let link = this.configService.baseURL + 'poll-options-by-question.php';
        let body = {
            optionQuestionId: this.selectedPoll.questionId
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    addOption() {
        let link = this.configService.baseURL + 'add-poll-option.php';
        let body = {
            optionQuestionId: this.selectedPoll.questionId,
            optionTitle: this.newOption.optionTitle
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    editOption() {
        let link = this.configService.baseURL + 'edit-option.php';
        let body = {
            optionId: this.editedOption.optionId,
            optionTitle: this.editedOption.optionTitle
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    getPolls() {
        let URL = this.configService.baseURL + 'poll-questions.php';
        return this.http.get(URL).map(res => res.json());;
    }

    addPoll() {
        let link = this.configService.baseURL + 'add-poll-question.php';
        let body = {
            questionTitle: this.newPoll.questionTitle, 
            questionDescription: this.newPoll.questionDescription,
            questionImage: this.newPoll.questionImage
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    editPoll() {
        let link = this.configService.baseURL + 'edit-poll-question.php';
        let body = {
            questionId: this.editedPoll.questionId, 
            questionTitle: this.editedPoll.questionTitle,
            questionDescription: this.editedPoll.questionDescription,
            questionImage: this.editedPoll.questionImage
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

}
