import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ConfigService } from '../config/config.service'
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    userId: any = {};
    newUser: any = {};
    selectedUser: any = {};
    editedUser: any = {};
    
    alertUserAdd: any = false;
    isSuccessAlertActive: any = false;

    constructor(public http: Http,
                public configService: ConfigService) 
    { 
    }

    deleteUser() {
        let link = this.configService.baseURL + 'delete-user.php';
        let body = {
            userId: this.selectedUser.userId
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    addUser()
        {
            let userBirthdate = this.configService.insert(this.newUser.userBirthdate, 2, '-');
            userBirthdate = this.configService.insert(userBirthdate, 5, '-');
            userBirthdate = userBirthdate.split('-')[2] + '-' + userBirthdate.split('-')[1] + '-' + userBirthdate.split('-')[0];
            console.log(this.newUser.userBirthdate);
            let link = this.configService.baseURL + 'add-user.php';
            let body = {
                        userId: this.newUser.userId, 
                        userName: this.newUser.userName, 
                        userEmail: this.newUser.userEmail,
                        userPasscode: this.newUser.userPasscode,
                        userCode: this.newUser.userCode,
                        userPhone: this.newUser.userPhone,
                        userBirthdate: userBirthdate
            };
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            return this.http.post(link, body, {headers: headers});
        }

    editUser()
        {
            let userBirthdate = this.configService.insert(this.editedUser.userBirthdate, 2, '-');
            userBirthdate = this.configService.insert(userBirthdate, 5, '-');
            userBirthdate = userBirthdate.split('-')[2] + '-' + userBirthdate.split('-')[1] + '-' + userBirthdate.split('-')[0];
            let userCode = this.configService.insert(this.editedUser.userCode, 3, '-');
            console.log(this.editedUser.userBirthdate);
            let link = this.configService.baseURL + 'edit-user.php';
            let body = {
                        userId: this.editedUser.userId, 
                        userName: this.editedUser.userName, 
                        userEmail: this.editedUser.userEmail,
                        userPasscode: this.editedUser.userPasscode,
                        userCode: this.editedUser.userCode,
                        userPhone: this.editedUser.userPhone,
                        userBirthdate: userBirthdate
            };
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            return this.http.post(link, body, {headers: headers});
        }
        
    getUsers() {
        let URL = this.configService.baseURL + 'users.php';
        return this.http.get(URL).map(res => res.json());;
    }

}
