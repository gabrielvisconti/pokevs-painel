import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ConfigService } from '../config/config.service'
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

    newCompany: any = {};

    companyId: any;
    selectedCompany: any = {};
    editedCompany: any = {};

    alertCompanyAdd: any = false;
    alertCompanyEdit: any = false;

    constructor(public http: Http,
                public configService: ConfigService) 
    { 
    }

    addCompany()
    {
        let link = this.configService.baseURL + 'add-company.php';
        let body = {
                    companyName: this.newCompany.companyName, 
                    companyEmail: this.newCompany.companyEmail,
                    companyPasscode: this.newCompany.companyPasscode,
                    companyDocument: this.newCompany.companyDocument,
                    companyPhone: this.newCompany.companyPhone,
                    companyAddress: this.newCompany.companyAddress,
                    companyContactName: this.newCompany.companyContactName,
                    companyContactDocument: this.newCompany.companyContactDocument,
                    companyOccupationArea: this.newCompany.companyOccupationArea,
                    companyDescription: this.newCompany.companyDescription
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }

    editCompany()
    {
        let link = this.configService.baseURL + 'edit-company.php';
        let body = {
                    companyId: this.editedCompany.companyId,
                    companyName: this.editedCompany.companyName, 
                    companyEmail: this.editedCompany.companyEmail,
                    companyPasscode: this.editedCompany.companyPasscode,
                    companyDocument: this.editedCompany.companyDocument,
                    companyPhone: this.editedCompany.companyPhone,
                    companyAddress: this.editedCompany.companyAddress,
                    companyContactName: this.editedCompany.companyContactName,
                    companyContactDocument: this.editedCompany.companyContactDocument,
                    companyOccupationArea: this.editedCompany.companyOccupationArea,
                    companyDescription: this.editedCompany.companyDescription
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(link, body, {headers: headers});
    }
        
    getCompanies() {
        let URL = this.configService.baseURL + 'companies.php';
        return this.http.get(URL).map(res => res.json());;
    }
        
    getCompanySuggestions() {
        let URL = this.configService.baseURL + 'company-suggestions.php';
        return this.http.get(URL).map(res => res.json());;
    }

}
